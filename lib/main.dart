import 'package:alcodes_on_board_flutter/root_app.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if (kDebugMode) {
    Fimber.plantTree(DebugTree());
  }

  runApp(RootApp());
}
