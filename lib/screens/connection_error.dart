import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';



class ConnectionError extends StatefulWidget {
  @override
  _ConnectionErrorState createState() => _ConnectionErrorState();
}

class _ConnectionErrorState extends State<ConnectionError> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Connection Error'),
      ),
      body: Center(
        child:AlertDialog(title: Text("No Internet Connection"),
                  content: Text("Please make sure you connecting to Internet."),
                  actions: <Widget>[
                    FlatButton(
                        child: Text('OK'),
                        onPressed: () {
                          SystemNavigator.pop();
                        })

        ])
      ),
    );
  }
}
