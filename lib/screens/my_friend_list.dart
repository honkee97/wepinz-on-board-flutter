import 'package:alcodes_on_board_flutter/models/form_models/friend_list_form_model.dart';
import 'package:alcodes_on_board_flutter/screens/friend_detail.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:alcodes_on_board_flutter/repository/auth_repo.dart';
import 'package:http/http.dart';
import 'dart:async';
import 'dart:convert';

/// TODO Issue on this screen:
/// - Show list of my friends. DONE
/// - Click list item go to my friend detail page. DONE

class MyFriendList extends StatefulWidget {
  @override
  _MyFriendListState createState() => _MyFriendListState();
}

class _MyFriendListState extends State<MyFriendList> {
  List friend = [];
  bool isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getFriend();
  }

  void getFriend() async {
    try {
      var response = await Dio().get("https://reqres.in/api/users?page=2");
      var items = (response.data)["data"];
      if (response.statusCode == 200) {
        setState(() {
          friend = items;
          isLoading = false;
        });
      } else {
        setState(() {
          friend = [];
          isLoading = false;
        });
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    getFriend();
    return Scaffold(
        appBar: AppBar(
          title: Text('My Friend List'),
        ),
        body: getBody());
  }

  Widget getBody() {
    if (friend.contains(null) || friend.length < 0 || isLoading == true) {
      return Center(child: CircularProgressIndicator());
    }
    return ListView.builder(
        itemCount: friend.length,
        itemBuilder: (context, index) {
          return getFriendList(friend[index]);

        });

  }

  Widget getFriendList(item) {
    var fullName = item['first_name'] + " " + item['last_name'];
    var email = item['email'];
    var avatar = item['avatar'];
    return Card(
      child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: ListTile(
            title: Row(children: <Widget>[
              Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(60 / 2),
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(avatar.toString())),
                  )),
              SizedBox(
                width: 20,
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      fullName.toString(),
                      style: TextStyle(fontSize: 18),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      email.toString(),
                      style: TextStyle(fontSize: 15, color: Colors.grey),
                    )
                  ])
            ]),
            onTap: () {
              Navigator.push(context,
                MaterialPageRoute(builder: (context) => FriendDetail(friendId:item['id']))
              );
              print(item);
            },
          )
      ),
    );
  }
}
