import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;

class FriendDetail extends StatefulWidget {
  final int friendId;
  const FriendDetail({Key? key, required this.friendId}) : super(key: key);
  @override
  _FriendDetailState createState() => _FriendDetailState();
}

class _FriendDetailState extends State<FriendDetail> {
  var friend;
  late Future _futureBuilder;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getDetail();
  }

  Future<String> getDetail() async {
    var friendID = widget.friendId;
    Response response;
    var dio = Dio();
    response = await dio.get('https://reqres.in/api/users/$friendID');
    var items = (response.data)["data"];
    friend = items;
    return Future.value("Data load successfully");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Friend Detail'),
        ),
        body: FutureBuilder<String>(
          future: getDetail(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (snapshot.hasData) {
              return showDetail();
            }
            return Center(
              child: Text('Error'),
            );
          },
        )
    );
  }
  Widget showDetail(){
    print("done");
    return Container(
        child: ListView(
          padding: const EdgeInsets.all(8),
          children: <Widget>[
            Container(
              height: 300,
              child: Image.network(
                friend['avatar'],
                width: 600,
                height: 300,
                fit: BoxFit.scaleDown,
              ),
            ),
            Container(
              height: 50,
              child: Center(
                  child: Text(' First Name: ' + friend['first_name'],
                      style: TextStyle(
                        fontSize: 20,
                      ))),
            ),
            Container(
              height: 50,
              child: Center(
                  child: Text(' Last Name: ' + friend['last_name'],
                      style: TextStyle(
                        fontSize: 20,
                      ))),
            ),
            Container(
              height: 50,
              child: Center(
                  child: Text(' Email: ' + friend['email'],
                      style: TextStyle(
                        fontSize: 20,
                      ))),
            )
          ],
        )
    );
  }
}
