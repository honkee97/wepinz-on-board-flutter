import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';



class TermOfUse extends StatefulWidget {
  @override
  _TermOfUseState createState() => _TermOfUseState();
}

class _TermOfUseState extends State<TermOfUse> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Terms of Use'),
        ),
        body: WebView(
          initialUrl: 'https://flutter.dev',
        ),
      );
  }
}
