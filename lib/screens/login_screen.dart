import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:alcodes_on_board_flutter/dialogs/app_alert_dialog.dart';
import 'package:alcodes_on_board_flutter/models/form_models/sign_in_form_model.dart';
import 'package:alcodes_on_board_flutter/repository/auth_repo.dart';
import 'package:alcodes_on_board_flutter/utils/app_focus_helper.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Utils.dart';

/// TODO Issue on this screen:
/// - Password field should hide the inputs for security purpose. DONE
/// - Login button is too small, make it fit the form width. DONE
/// - Should show loading when calling API, and hide loading when server responded.
/// - Wrong login credential is showing as Toast, Toast will auto dismiss
///   and user will miss out the error easily, should use popup dialog. DONE

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _formModel = SignInFormModel();
  final log = FimberLog("MY_TAG");

  Future<void> _onSubmitFormAsync() async {
    // Hide keyboard.
    AppFocusHelper.instance.requestUnfocus();


    // Validate form and save inputs to form model.
    if (_formKey.currentState?.validate() ?? false) {
      _formKey.currentState!.save();

      // Call API.
      var alertDialogStatus = AppAlertDialogStatus.error;
      var alertDialogMessage = '';

      try {
        final repo = AuthRepo();
        final apiResponse = await repo.signInAsync(
          email: _formModel.email,
          password: _formModel.password,
        );

        // Login success, store user credentials into shared preference.
        final sharedPref = await SharedPreferences.getInstance();

        await sharedPref.setString(
            SharedPreferenceKeys.userEmail, _formModel.email);
        await sharedPref.setString(
            SharedPreferenceKeys.userToken, apiResponse.data!.token);

        // Navigate to home screen.
        Navigator.of(context)
            .pushNamedAndRemoveUntil(AppRouter.home, (route) => false);

        return;
      } catch (ex) {
        Fimber.e('d;;Error request sign in.', ex: ex);

        alertDialogMessage = '$ex';
      }

      if (alertDialogMessage.isNotEmpty) {
        // Has message, show alert dialog.
        final appAlertDialog = AppAlertDialog();

        appAlertDialog.showAsync(
          context: context,
          status: alertDialogStatus,
          message: alertDialogMessage,
        );
      }
    }
  }

  bool isHidePassword = true;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(appConst.kDefaultPadding),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                initialValue: 'eve.holt@reqres.in',
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                onSaved: (newValue) => _formModel.email = newValue ?? '',
                validator: (value) {
                  if (value?.isEmpty ?? true) {
                    return 'Required field.';
                  }
                  return null;
                },
                onFieldSubmitted: (value) => _onSubmitFormAsync(),
                decoration: InputDecoration(
                  hintText: 'Email',
                ),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              TextFormField(
                obscureText: isHidePassword,
                initialValue: 'cityslicka',
                textInputAction: TextInputAction.done,
                onSaved: (newValue) => _formModel.password = newValue ?? '',
                onFieldSubmitted: (value) => _onSubmitFormAsync(),
                validator: (value) {
                  if (value?.isEmpty ?? true) {
                    return 'Required field.';
                  }

                  return null;
                },
                decoration: InputDecoration(
                    hintText: 'Password',
                    suffixIcon: InkWell(
                      onTap: _togglePasswordView,
                      child: Icon(
                        Icons.visibility,
                      ),
                    )),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              ElevatedButton(
                onPressed: _onSubmitFormAsync,
                child: Text('Login', style: TextStyle(fontSize: 24)),
                style: ElevatedButton.styleFrom(fixedSize: const Size(200, 60)),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(text: "Term Of Use", style: TextStyle(
                          color: Colors.lightBlue[900],
                          fontWeight:FontWeight.bold,
                          fontSize: 20,
                          decoration: TextDecoration.underline,
                        ),recognizer: TapGestureRecognizer()..onTap=()async{
                          Navigator.of(context).pushNamed(AppRouter.termOfUse);
                        })
                      ]
                  )
              ),
              SizedBox(height: appConst.kDefaultPadding),
              ElevatedButton(
                onPressed:() async {
                  final result = await Connectivity().checkConnectivity();
                  showConnectivitySnackBar(result);
                },
                child: Text('Check Connection', style: TextStyle(fontSize: 15)),
                style: ElevatedButton.styleFrom(fixedSize: const Size(200, 60)),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void showConnectivityError(ConnectivityResult result){
    final hasInternet = result != ConnectivityResult.none;
    final message = hasInternet ?'You have ${result.toString()}' : 'no internet connection';
    final color = hasInternet? Colors.green : Colors.red;

    Utils.showTopSnackBar(context,message,color);
  }


  void _togglePasswordView() {
    if (isHidePassword == true) {
      isHidePassword = false;
    } else {
      isHidePassword = true;
    }
    setState(() {});
  }
}
