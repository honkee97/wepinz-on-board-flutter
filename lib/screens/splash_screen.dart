import 'dart:async';

import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      var networkResult = await Connectivity().checkConnectivity();
      // if (networkResult != ConnectivityResult.wifi || networkResult != ConnectivityResult.mobile){
      //   print("no connection");
      //   Navigator.of(context).pushNamed(AppRouter.connectionError);
      // }
      // Check if user is logged in.
      final sharedPref = await SharedPreferences.getInstance();

      // Short form:
      // final isLoggedIn = (sharedPref.getString(SharedPreferenceKeys.userEmail)?.isNotEmpty ?? false);

      // Or easy read:
      final userEmail = sharedPref.getString(SharedPreferenceKeys.userEmail);
      final isLoggedIn = (userEmail != null && userEmail.isNotEmpty);

      String routeName;
      // if (networkResult != ConnectivityResult.wifi ||
      //     networkResult != ConnectivityResult.mobile) {
      //   print("no connection");
      //   Navigator.of(context).pushNamed(AppRouter.connectionError);
      // }else
        if (isLoggedIn) {
        routeName = AppRouter.home;
        Navigator.of(context)
            .pushNamedAndRemoveUntil(routeName, (route) => false);
      } else {
        routeName = AppRouter.login;
        Navigator.of(context)
            .pushNamedAndRemoveUntil(routeName, (route) => false);
      }
    });

    super.initState();
  }

  // _checkInternetConnectivity() async {
  //   var networkResult = await Connectivity().checkConnectivity();
  //   if (networkResult == ConnectivityResult.none) {
  //     return AlertDialog(
  //         title: Text("No Internet Connection"),
  //         content: Text("Please make sure you connecting to Ineternet."),
  //         actions: <Widget>[
  //           FlatButton(
  //               child: Text('OK'),
  //               onPressed: () {
  //                 Navigator.of(context).pop();
  //               })
  //         ]);
  //   }
  //   }
showDialog(){
    return AlertDialog(
                title: Text("No Internet Connection"),
                content: Text("Please make sure you connecting to Ineternet."),
                actions: <Widget>[
                  FlatButton(
                      child: Text('OK'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      })
                ]);
}
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
