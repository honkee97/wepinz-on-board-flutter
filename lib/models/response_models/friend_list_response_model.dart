class FriendListResponseModel {
  String token;

  FriendListResponseModel({
    this.token = '',
  });

  factory FriendListResponseModel.fromJson(Map<String,dynamic>json){
    return FriendListResponseModel(token:json["token"]!=null?json["token"]:"");
  }
}