class SignInResponseModel {
  String token;

  SignInResponseModel({
    this.token = '',
  });

  factory SignInResponseModel.fromJson(Map<String,dynamic>json){
    return SignInResponseModel(token:json["token"]!=null?json["token"]:"");
  }
}
