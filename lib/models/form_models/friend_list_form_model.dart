class FriendListFormModel {
  String email;
  String name;

  FriendListFormModel({
    this.email = '',
    this.name = '',
  });

  Map <String,dynamic> toJson(){
    Map<String,dynamic> map = {
      'email':email.trim(),
      'name':name.trim(),
    };
    return map;
  }
}